#!/bin/bash

docker-compose down
docker-compose build
docker-compose up -d

netsh interface portproxy add v4tov4 listenport=8081 connectaddress=192.168.99.100 connectport=8081 protocol=tcp
